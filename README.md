BDI Pharma Order Inquiry
=====
This module was developed for [BDI Pharma](http://bdipharma.com) at [I-SITE, Inc.](http://i-site.com).

About the Module
-----
This module allows pharmacists and manufacturers to browse through a [robust online catalog](http://bdipharma.com/product), add products to an Order Inquiry, and submit the inquiry to a BDI representative. This allows users to browse product offerings at their leisure and connect with sales representatives on their own pace.

Note: This module does NOT capture sensitive information, such as billing or personal details. The purpose of this module is for sales representatives to obtain initial contact and then follow up with the customer.

Features
-----
* Administration panel for site admin to enter mail settings.
* Integration with Flag module to flag field collection items within a product content type.
* Integration with Views module to capture flagged items that can be sent to representatives as a list.
* Stores information as a node and via e-mail to sales representatives.


Dependencies
-----
* Drupal 7.x
* [Views 7.x](https://www.drupal.org/project/views) module
* [Flag 7.x](https://www.drupal.org/project/flag) module