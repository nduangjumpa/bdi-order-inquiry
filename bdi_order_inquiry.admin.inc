<?php
function bdi_order_inquiry_admin($form, &$form_state) {
	// $form = array();

	$form['to_email'] = array (
		'#type' => 'textfield',
		'#title' => 'To e-mail address',
    '#default_value' => variable_get('to_email', 'default value'),
		'#required' => TRUE,
	);
	$form['from_email'] = array (
		'#type' => 'textfield',
		'#title' => 'From e-mail address',
    '#default_value' => variable_get('from_email', 'default value'),
		'#required' => TRUE,
	);

	$form['subject_email'] = array (
		'#type' => 'textfield',
		'#title' => 'Subject of email',
    '#default_value' => variable_get('subject_email', 'default value'),
		'#required' => TRUE,
	);

	return system_settings_form($form);
}
?>